# EventLib

<script>
    let eventid = "{{ $eventid }}";
    let landing_hash = "{{$landing_hash}}";

    window.addEventListener("load", function(event){
        weblib.init(eventid); // save evend_id in lib
        weblib.initFbp();    // send Fbp
        weblib.store('pageview');  // send event name
        weblib.initCheckout(landing_hash); // add to .initCheckout link to appstore (web2app)
    })
    

</script>

<!-- Use in Quiz -> send number of pass questions -->
<script>
 weblib.storeQuiz('quizStep');
</script>
