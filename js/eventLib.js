
function getFbClientId() {
    let result = /_fbp=(fb\.1\.\d+\.\d+)/.exec(window.document.cookie);
    if (!(result && result[1])) {
        return null;
    }
    return result[1];
}


function getFbClickId() {
    let result = /_fbc=(fb\.1\.\d+\.\d+)/.exec(window.document.cookie);
    if (!(result && result[1])) {
        return null;
    }
    return result[1];
}

var fbp = getFbClientId();
var fbc = getFbClickId();

var event;

var weblib=
    {
        init:function (eventId) {
            event = eventId;
            return event
        },
        initFbp: function(){
            if(fbp == null){
                fbp = getFbClientId();

            } else if(fbp == null){
                setTimeout(function () {
                    fbp = getFbClientId();
                }, 1000)
            }
            $.ajax({
                type: "POST",
                url: "https://web2app.fx2.io/event/store",
                data: {
                    _method: 'POST',
                    event_id: event,
                    event_type: 'fbp',
                    fbp: fbp
                },
                success: function (html) {
                    if(html.res == 'success') {
                        return false
                    }
                }
            });
        },
        store: function (eventName) {
            $.ajax({
                type: "POST",
                url: "https://web2app.fx2.io/event/store",
                data: {
                    _method: 'POST',
                    event_id: event,
                    event_type: eventName
                },
                success: function (html) {
                    if(html.res == 'success') {
                        return false
                    }
                }
            });
        },
        storeQuiz: function (quizStep) {
            $.ajax({
                type: "POST",
                url: "https://web2app.fx2.io/event/store",
                data: {
                    _method: 'POST',
                    event_id: event,
                    event_type: 'quizview',
                    quiz_item: quizStep,
                },
                success: function (html) {
                    if(html.res == 'success') {
                        return false
                    }
                }
            });
        },
        initCheckout: function (landing_hash) {
            $('.initCheckout').attr('href', 'https://web2app.fx2.io/checkout?event_id=' + event + '&landing=' + landing_hash);
        }
        
    };

console.log('test');